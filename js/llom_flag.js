/**
 * @file
 * Add the language flag to menu items.
 *
 * SC 21 Mar 2024 15:47:23 stefano.
 */
(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.llomFlag = {
    attach: function (context, settings) {

      if (settings.llom.llom_flag != 'never') { settings.llom.languages_id.forEach(addLang); }

      function addLang(lang) {

        var llomClass = ".llom-flag-" + lang + ".llom-need-flag";
        var llomTitle = "";
        var flag = "";

        llomTitle = document.querySelectorAll(llomClass);

        for (let index = 0; index < llomTitle.length; index++) {

          flag = '<img src="' + settings.llom.pub_files + lang + '.png" alt="' + lang + '" title="' + $(llomTitle[index]).text().trim() + '" class="llom-flags" tabindex="-1">';

          if (!$(llomTitle[index]).children().length) {
            $(llomTitle[index]).wrapInner('<span class="llom-span" style="display: inline-flex;"></span>');
          }

          let llomToAdd = $(llomTitle[index]).children();

          switch (settings.llom.llom_flag) {
            case 'never':
              break;

            case 'before':
              $(llomToAdd).prepend(flag, "&nbsp;");
              break;

            case 'after':
              $(llomToAdd).append("&nbsp;", flag);
              break;

            case 'notext':
              llomTitle[index].innerHTML = flag;
              break;

            default:
              break;
          }

          /*
           * Remove this class to avoid a recursive image.
           *
           */
          $(llomTitle[index]).removeClass("llom-need-flag");
        }
      };
    }
  };
})(jQuery, Drupal, drupalSettings);
