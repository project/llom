Language Links On Menu
======================

A language switcher to be placed as a menu item.

Differently the majority of language switchers who uses a block, this
module uses a tree menu with the current language as principal item and the
child items for the other languages.

In this manner it's possible put the switcher in one or more menus and if
necessary, afterwards with the Menu UI, in any place in the tree. The
module only puts the links and then uses the rules of the menu/theme, so
also permits the use of hooks, class, themes, etc. as part of a normal
menu.

Instructions
------------

Use your preferred way to install the module (unpack the zip into modules
folder, composer/drush, administration "Extend" page, etc.).

Once installed, before configure it, some checks are needed:
- at least 2 language configured in /admin/config/regional/language;
- in "Interface text language detection" and/or "Content language
  detection" in /admin/config/regional/language/detection, the URL
  detection method has enabled and with priority on other methods.

With the language configured, it's possible configure Language Links On
Menu from /admin/config/regional/llom:
- select the menus where display the switcher;
- according to "Detection and selection" above, select the language type.
  If both suit the site, a priority has done to "Interface text";
- set the weight, principally to put the switcher at the begin or the end
  of the items;
- select the test format, full for "English" or short for 2 characters ISO
  like "EN";
- select if insert a flag and where places it compared to the text or only
  the flag without text (the files must be put in the site public://
  folder).

After has configured the module, if necessary uses Menu UI for a finest
position of the switcher. Almost all UI settings are preserved.

The module preserves the theme classes, but adds new classes for menu and
language for a precise styling of the switcher.

Attention
---------

About the flags. The module doesn't check if the language flag has loaded
in the public:// folder or if the filename is correct (ISO code dot png,
for example en.png) or its dimensions are right, etc. All these points are
responsability of the site or theme developer. More, the JQuery script who
insert the flags, could add some CSS class/style and/or HTML tags for shows
the flags.

Do a menu reset from Menu UI, if manually has configured some menus, before
remove the menu selection from module configuration, otherwise the switcher
could be showed again.

If the menu doesn't use levels, for example, the child items (the other
languages) will not showed.

When a language has added or removed, the module does a rebuild of all
menus.
