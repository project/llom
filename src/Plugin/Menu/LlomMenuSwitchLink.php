<?php

namespace Drupal\llom\Plugin\Menu;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Menu\StaticMenuLinkOverridesInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A menu link that displays language switcher.
 */
class LlomMenuSwitchLink extends LlomMenuLink {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a new points menu link.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Menu\StaticMenuLinkOverridesInterface $static_override
   *   The static override storage.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, StaticMenuLinkOverridesInterface $static_override, LanguageManagerInterface $language_manager, PathMatcherInterface $path_matcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $static_override, $language_manager, $path_matcher);

    if ($this->getMetaData()['langcode'] == $this->langCurrentId) {
      $this->setEnabled(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu_link.static.overrides'),
      $container->get('language_manager'),
      $container->get('path.matcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {

    $this->getSwitchLink();

    if (!$this->isEnabled()) {
      return $this->pluginDefinition['title'];
    }

    $config = \Drupal::config('llom.admin_settings');

    $lang_current_id = $this->getMetaData()['langcode'];

    switch ($config->get('llom_code')) {
      case 'full':
        $lang_current_title = $this->t('@lang', ['@lang' => $this->link[$lang_current_id]['title']], ['langcode' => $lang_current_id]);
        break;

      case 'short':
        $lang_current_title = $lang_current_id;
    }

    return $lang_current_title;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrlObject($title_attribute = TRUE) {

    $this->getSwitchLink();

    $url = $this->link[$this->getMetaData()['langcode']]['url'];

    $url->mergeOptions($this->getOptions());

    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return [$this->pluginId];
  }

}
