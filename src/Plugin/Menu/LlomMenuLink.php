<?php

namespace Drupal\llom\Plugin\Menu;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Menu\MenuLinkDefault;
use Drupal\Core\Menu\StaticMenuLinkOverridesInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A menu link that displays language switcher.
 */
class LlomMenuLink extends MenuLinkDefault {

  /**
   * The current language id.
   *
   * @var string
   */
  protected $langCurrentId;

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Language switch links for active route.
   *
   * NULL, if never initialized. FALSE, if unsuccessfully initialized. An array
   * of language switch links, if successfully initialized.
   *
   * @var array|null|false
   * @phpstan-var array<string,mixed>|null|false
   */
  protected $link = NULL;

  /**
   * Constructs a new points menu link.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Menu\StaticMenuLinkOverridesInterface $static_override
   *   The static override storage.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, StaticMenuLinkOverridesInterface $static_override, LanguageManagerInterface $language_manager, PathMatcherInterface $path_matcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $static_override);
    $this->langCurrentId = $language_manager->getCurrentLanguage()->getId();
    $this->languageManager = $language_manager;
    $this->pathMatcher = $path_matcher;

    $config = \Drupal::config('llom.admin_settings');

    if ($plugin_id == 'llom.menu_link' || $plugin_id == 'llom.menu_switch_link') {
      $this->setEnabled(0);
    }

    if (!isset($this->getMetaData()['langcode'])) {
      $this->setWeight($config->get('llom_weight'));
      $this->setEnabled(1);

    }

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu_link.static.overrides'),
      $container->get('language_manager'),
      $container->get('path.matcher')
    );
  }

  /**
   * Initializes the "link" property.
   *
   * SC 18 Mar 2024 19:38:48 stefano.
   */
  public function getSwitchLink() {

    if ($this->link !== NULL) {
      return;
    }

    $config = \Drupal::config('llom.admin_settings');
    $config_negotiation = \Drupal::config('language.types')->get('negotiation');

    $route_match = \Drupal::routeMatch();
    // If there is no route match, for example when creating blocks on 404 pages
    // for logged-in users with big_pipe enabled using the front page instead.
    if ($this->pathMatcher->isFrontPage() || !$route_match->getRouteObject()) {
      // We are skipping the route match on both 404 and front page.
      // Example: If on front page, there is no route match like when creating
      // blocks on 404 pages for logged-in users with big_pipe enabled, use the
      // front page.
      $url = Url::fromRoute('<front>');
    }
    else {
      $url = Url::fromRouteMatch($route_match);
    }

    $types = $this->languageManager->getLanguageTypes();

    // Intersect llom types and available to have the only active.
    //
    // In the meantime could be added or better removed a type, so we need a
    // clean array.
    //
    // SC 16 Mar 2024 13:09:03 stefano.
    //
    $active_types = array_intersect($config->get('llom_ltype'), $types);

    foreach ($active_types as $type) {

      if (!isset($config_negotiation[$type]['enabled']['language-url'])) {
        unset($active_types[$type]);
      }
      else {
      }
    }

    switch (count($active_types)) {
      case 0:
        $this->setEnabled(0);
        break;

      case 1:
        $this->link = $this->languageManager->getLanguageSwitchLinks(array_shift($active_types), $url)->links;
        break;

      default:
        // For more type use the interface (default?).
        //
        // SC 16 Mar 2024 15:00:08 stefano.
        //
        $this->link = $this->languageManager->getLanguageSwitchLinks(LanguageInterface::TYPE_INTERFACE, $url)->links;
        break;
    }

  }

  /**
   * Set link enabled status.
   *
   * SC 20 Mar 2024 18:25:09 stefano.
   *
   * @param bool $bool
   *   The status. Use 0/1.
   */
  public function setEnabled($bool) {
    $this->pluginDefinition['enabled'] = $bool;
  }

  /**
   * Set link weight.
   *
   * SC 20 Mar 2024 18:25:09 stefano.
   *
   * @param bool $weight
   *   The weight.
   */
  public function setWeight($weight) {
    $this->pluginDefinition['weight'] = $weight;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {

    $this->getSwitchLink();

    if (!$this->isEnabled()) {
      return $this->pluginDefinition['title'];
    }

    $config = \Drupal::config('llom.admin_settings');

    switch ($config->get('llom_code')) {
      case 'full':
        $lang_current_title = $this->t('@lang', ['@lang' => $this->link[$this->langCurrentId]['title']], ['langcode' => $this->langCurrentId]);
        break;

      case 'short':
        $lang_current_title = $this->langCurrentId;
    }

    return $lang_current_title;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrlObject($title_attribute = TRUE) {

    $config = \Drupal::config('llom.admin_settings');

    $this->getSwitchLink();

    $url = $this->link[$this->langCurrentId]['url'];

    $options = $this->getOptions();

    // Added the llom-need-flag class to be removed in the js scritp to avoid a
    // recursive imgage/flag additions.
    //
    $options['attributes']['class'] = [
      "llom-flag-" . $this->langCurrentId,
      "llom-title-" . $config->get('llom_code'),
      "llom-need-flag",
    ];

    $options['attributes']['lang'] = $this->langCurrentId;

    $url->mergeOptions($options);

    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return [$this->pluginId];
  }

}
