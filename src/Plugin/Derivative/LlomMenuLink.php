<?php

namespace Drupal\llom\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides menu links to switch to another language.
 */
class LlomMenuLink extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a new LanguageSwitcherLink instance.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(LanguageManagerInterface $language_manager) {
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed $base_plugin_definition
   * @phpstan-return array<string, mixed>
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];

    $config = \Drupal::config('llom.admin_settings');

    if (empty($config->get('llom_menu')) || !$this->languageManager->isMultilingual()) {
      return $links;
    }
    else {

      foreach ($config->get('llom_menu') as $key => $menu) {

        if (!$menu) {
          if (isset($links['llom.menu_link_' . $key])) {
            unset($links['llom.menu_link_' . $key]);
          }

          continue;
        }

        $links['llom.menu_link_' . $key] = [
          'title' => 'unset llom language',
          'menu_name' => $menu,
          'enabled' => 0,
          'weight' => $config->get('llom_weight'),
          /* 'options' => [
            'attributes' => [
              'class' => ["llom-flag-" . $lang_current_id],
              'lang' => $lang_current_id,
            ],
          ], */
        ] + $base_plugin_definition;
      }
    }

    return $links;
  }

}
