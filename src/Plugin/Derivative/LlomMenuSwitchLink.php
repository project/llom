<?php

namespace Drupal\llom\Plugin\Derivative;

/**
 * Provides menu links to switch to another language.
 */
class LlomMenuSwitchLink extends LlomMenuLink {

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed $base_plugin_definition
   * @phpstan-return array<string, mixed>
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];

    $config = \Drupal::config('llom.admin_settings');

    if (empty($config->get('llom_menu')) || !$this->languageManager->isMultilingual()) {
      return $links;
    }
    else {

      $langcodes = $this->languageManager->getLanguages();

      foreach ($config->get('llom_menu') as $key => $menu) {

        if (!$menu) {
          if (isset($links['llom.menu_link_' . $key])) {
            unset($links['llom.menu_link_' . $key]);
          }

          continue;
        }

        foreach ($langcodes as $key_lang => $lang) {

          $links['llom.menu_link_' . $key . "-" . $key_lang] = [
            'title' => $lang->getName(),
            'menu_name' => $menu,
            'parent' => 'llom.menu_link:llom.menu_link_' . $key,
            'enabled' => 1,
            'options' => [
              'language' => $lang,
              'attributes' => [
                'class' => [
                  "llom-flag-" . $key_lang,
                  "llom-title-" . $config->get('llom_code'),
                  "llom-need-flag",
                ],
                'lang' => $key_lang,
              ],
            ],
            'metadata' => [
              'language' => $lang->getName(),
              'langcode' => $lang->getId(),
              // 'langtype' => $type,
            ],
          ] + $base_plugin_definition;
        }

      }
    }

    return $links;
  }

}
