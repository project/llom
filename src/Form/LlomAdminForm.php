<?php

namespace Drupal\llom\Form;

/**
 * @file
 * Llom configuration form.
 *
 * Date: 23 Feb 2024 19:11:24
 * File: LlomAdminForm.php
 * Author: stefano.
 */

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\language\ConfigurableLanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures forms module settings.
 */
class LlomAdminForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The file url generator service.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * The menu link manager.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $menuLinkManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   * @param \Drupal\Core\Utility\FileUrlGeneratorInterface $fileUrlGenerator
   *   The file URL generator service.
   * @param \Drupal\Core\Menu\MenuLinkManagerInterface $menuLinkManager
   *   The menu link manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LanguageManagerInterface $languageManager, FileUrlGeneratorInterface $fileUrlGenerator, MenuLinkManagerInterface $menuLinkManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $languageManager;
    $this->fileUrlGenerator = $fileUrlGenerator;
    $this->menuLinkManager = $menuLinkManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Load the service from the container.
    return new static(
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('file_url_generator'),
      $container->get('plugin.manager.menu.link')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'llom_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'llom.admin_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get the storage handler for menu entities.
    $menuStorage = $this->entityTypeManager->getStorage('menu');
    $menus_tree = $menuStorage->loadMultiple();

    $menu_option = [];

    foreach ($menus_tree as $menu) {
      $menu_option[$menu->id()] = $menu->label();
    }

    $config = $this->config('llom.admin_settings');
    $form['menu'] = [
      '#type' => 'checkboxes',
      '#options' => $menu_option,
      '#title' => $this->t('Menu'),
      '#description' => $this->t('Select the menus. Please, before check if the menu supports sub-menu.'),
      '#default_value' => ($config->get('llom_menu') ?? []),
    ];

    $language_manager = $this->languageManager;

    if ($language_manager instanceof ConfigurableLanguageManagerInterface) {
      $info = $language_manager->getDefinedLanguageTypesInfo();
      $configurable_types = $language_manager->getLanguageTypes();
      foreach ($configurable_types as $type) {
        $ltype_option[$type] = $this->t('@type', ['@type' => $info[$type]['name']]);
      }
    }

    $form['ltype'] = [
      '#type' => 'checkboxes',
      '#options' => $ltype_option,
      '#title' => $this->t('Language type'),
      '#description' => $this->t('The definite types text for languages.'),
      '#default_value' => ($config->get('llom_ltype') ?? []),
    ];

    $form['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight'),
      '#description' => $this->t('Menu item weight of <em>current</em> language.'),
      '#delta' => 100,
      '#default_value' => (int) $config->get('llom_weight') ?? 50,
    ];

    /**
     * @var Drupal\Core\Language\LanguageManager
     */
    $language_current = $language_manager->getCurrentLanguage();

    $form['code'] = [
      '#type' => 'select',
      '#options' => ['full' => $this->t('Full'), 'short' => $this->t('Short')],
      '#title' => $this->t('Text format'),
      '#description' => $this->t('If the menu item text has a <i>full</i> (@full) or <i>short</i> (@short) format.',
      [
        '@full' => $language_current->getName(),
        '@short' => $language_current->getId(),
      ]),
      '#default_value' => ($config->get('llom_code') ?? 'full'),
    ];

    $pub_files = $this->fileUrlGenerator
      ->generateAbsoluteString('public://');

    $form['flag'] = [
      '#type' => 'radios',
      '#title' => $this->t('Flags'),
      '#options' => [
        'never' => $this->t('Never'),
        'before' => $this->t('Before'),
        'after' => $this->t('After'),
        'notext' => $this->t('Only'),
      ],
      '#default_value' => ($config->get('llom_flag') ?? 'never'),
      '#description' => $this->t('Put your images inside of %path as <i>language ISO code</i>.png (like %short.png for @full). <i>No check will done if image/language esists.</i>',
      [
        '%path' => $pub_files,
        '%short' => $language_current->getId(),
        '@full' => $language_current->getName(),
      ]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('llom.admin_settings')
      ->set('llom_menu', $form_state->getValue('menu'))
      ->set('llom_weight', $form_state->getValue('weight'))
      ->set('llom_code', $form_state->getValue('code'))
      ->set('llom_ltype', $form_state->getValue('ltype'))
      ->set('llom_flag', $form_state->getValue('flag'))
      ->save();
    parent::submitForm($form, $form_state);

    // Rebuild the menu.
    //
    // SC 11 Mar 2024 17:00:38 stefano.
    //
    $this->menuLinkManager->rebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

}
